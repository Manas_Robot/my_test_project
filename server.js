var express = require('express');

var app = express();

const PORT = 3000;


var middleWare ={
    logger : function(req,res,next){
        console.log('Request '+new Date().toString()+'  '+req.method +' '+req.originalUrl);
        next();
    }
}

app.use(middleWare.logger);
app.get('/',function(req,res){
    res.send('Good evening');
})

app.get("/about",function(req,res){   
    res.send('About Us!');
})


app.listen(PORT,()=>{
    console.log('Express server started at port '+PORT+'!');
});